# ik maak een paar functies die je later kan invullen als je wil
# onderaan staat de hoofdlus die de andere functies zal aanroepen

def getDataOutOfNavision():
    """Kan data uit Navision halen. Misschien parameters nodig voor datum of een locatie om de data weg te schrijven?
    """
    pass

def loadExportedData():
    """Navision export inladen
    """
    pass

def loadCostData():
    """kostendat in een of ander formaat inladen
    """
    pass

def createLINGOInput(EFI, VKC, costData, outputFile):
    """Krijgt EFI, VKC en kostenData binnen en maakt de berekening van de allocatiematrix
    
    Arguments:
        EFI {[type]} -- [description]
        VKC {[type]} -- [description]
        costData {[type]} -- [description]
    """
    pass

def runLINGO(inputFile):
    """(automatisch) runnen van LINGO
    
    Arguments:
        inputFile {[type]} -- [description]
    """
    pass

def analyseLINGO(LINGOoutputfile):
    """Lees de output van lingo en analyseer en maak plotjes
    
    Arguments:
        LINGOoutputfile {[type]} -- [description]
    """
    pass

if __name__ == "__main__":
    getDataOutOfNavision()
    efi, vkc = loadExportedData()
    costData = loadCostData()

    LINGOtekstbestandje = "lingo.txt"
    createLINGOInput(efi, vkc, costData, LINGOtekstbestandje)

    runLINGO(inputFile=LINGOtekstbestandje)

    anlayseLINGO(LINGOoutputfile="LINGOout.txt")