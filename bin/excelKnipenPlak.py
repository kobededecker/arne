# Ik probeer de lingo input automatisch aan te maken op basis van je excelfile.
import pandas as pd
import numpy as np
import os

def excelToLINGO(excelFile):
    LINGOtemplate = """
    MODEL:


    SETS:

    EFINUMMER / 
    {EFIstring}
    /: CAPACITY;

    VKCNUMMER / 
    {VKCstring}
    /   : DEMAND;

    LINKS( EFINUMMER , VKCNUMMER ): ALLOCATIEKOST, VOLUME;

    ENDSETS

    ! The objective;

    MIN = @SUM( LINKS( I, J):

    ALLOCATIEKOST( I, J) * VOLUME( I, J));

    ! The demand constraints;

    @FOR( VKCNUMMER ( J):

    @SUM( EFINUMMER ( I): VOLUME( I, J)) =

        DEMAND( J));

    ! The capacity constraints;

    @FOR( EFINUMMER ( I):

    @SUM( VKCNUMMER ( J): VOLUME( I, J)) <=

        CAPACITY( I));

    ! Here is the data;

    DATA:

    CAPACITY = 
    {Capacity}
    ;

    DEMAND = 
    {Demand}
    ;

    ALLOCATIEKOST = 	
    {Allocatie}	

    ;

    ENDDATA

    END
    """

    excelFile = pd.ExcelFile(excelFile)
    efitab = pd.read_excel(excelFile, sheet_name="EFI", header=5)
    EFIstring = efitab["EFI-nummer"].tolist()
    Capacity = efitab["aantal zakken (equivalent)"].astype("str").tolist()
    
    vkctab = pd.read_excel(excelFile, sheet_name="VKC", header=5)
    VKCstring = vkctab["VKC-nummer"].tolist()
    Demand = vkctab["aantal zakken (equivalent)"].astype("str").tolist()

    sheets = excelFile.sheet_names
    sheets_cost = [x for x in sheets if x.endswith("kost")]

    # we halen uit elke "kost" tab de matrix
    kosten = []
    for sheet in sheets_cost:
        print(sheet)
        df = pd.read_excel(excelFile, sheet_name=sheet, header=5, index_col=0)
        selectie = df.iloc[:len(Capacity), :len(Demand)]
        print(selectie)
        kosten.append(selectie.values)

    # tellen we die kosten op
    totale_kost = np.sum(np.array(kosten), axis=0)

    # nu zetten we dat om naar strings
    totale_kost_str = "\n\t".join(["\t".join(row.astype("int").astype("str")) for row in totale_kost])

    # nu kunnen we die template invullen, elke string tussen {} kunnen we met de 'format' functie invullen
    LINGOtekst = LINGOtemplate.format(Capacity="\t".join(Capacity), Demand="\t".join(Demand), EFIstring="\t".join(EFIstring), VKCstring="\t".join(VKCstring), Allocatie=totale_kost_str)

    return LINGOtekst


if __name__ == "__main__":
    LINGOtekst = excelToLINGO(r".\excelInput\LINGO3EFI.xlsx")

    with open(os.path.join("LINGOinput", "input0001.txt"), mode="w") as fileke: # 
        fileke.write(LINGOtekst)