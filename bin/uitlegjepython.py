# Hoi Arne! een korte uitleg: Python gebruikt # als commentaar teken, dus dat wordt al niet meegetld.
# Python werkt met 'libraries', waar specifieke functionaliteit werd in ondergebracht, deze moeten geimporteerd worden.
# Dit zijn de belangrijkste:
import os # om files te beheren op je computer
import pandas as pd # om tabulaire data te openen en weg te schrijven en te analyseren
import glob # om files te vinden in een bepaalde folder

# python werkt met indentaties op functies en loops en if'lussen te opene en te sluiten
for item in ["a","b","c"]:
    print(item)
    if item=="a":
        print("item = a")


# functies starten met dev en je kan argumenten meegeven
def functie1(arg1, arg2=3): # arg1 moet altijd opgegeven worden, arg2 moet niet, want heeft al een default waarde 3
    print arg1+arg2

functie1(5, 4) # = 9
functie1(5) # = 8 want 3 is default

